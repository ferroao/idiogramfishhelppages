### load iteration
# R -e 'saveRDS(1,"i.rds")'
# R -e 'saveRDS(list(),"finaldb500.rds")'

i <- readRDS("i.rds")
print(i)

tryCatch(pid <- readRDS(file = "pid.rds"), error = function(e) {
  NA
}, warning = function(w) {
  ""
})
if (exists("pid")) {
  library(tools)
  print(pid)
  tools::pskill(pid, SIGKILL) # does not work in Rstudio
}

#
Sys.sleep(1)
library(data.table)

file_list <- readRDS("file_list.rds")
print(paste("Processing", file_list[i]))
file.db <- fread(file_list[i],
  header = TRUE, sep = ",",
  dec = "." # , fill = TRUE
)
file.db[is.na(file.db)] <- ""

currentDate <- gsub("CRANlogs/|.csv.gz", "", file_list[i])

readIPs <- readRDS(paste0("IPs2/", currentDate, ".rds"))
# head(readIPs)
microbenchmark::microbenchmark(
  tmp.logs <- file.db[
    interaction(file.db[, c("ip_id", "r_version", "r_os")], drop = T) %in%
      interaction(readIPs[, .SD[numberOfPacs < 200], .SDcols = c("ip_id", "r_version", "r_os")], drop = T) # [, ] )
  ],
  times = 1
)
# print( (pryr::mem_used() ) )
remove(file.db)
remove(readIPs)
gc()

package.name <- "idiogramFISH"
tmp.logs <- tmp.logs[, .SD[package == package.name]]
# str(tmp.logs)
tmp.logs$date <- as.character(tmp.logs$date)
# str(final.db)
final.db <- readRDS("db200.rds")
# tail(final.db)
final.db <- rbind(final.db, tmp.logs)

print(dim(final.db))

saveRDS(final.db, file = "db200.rds")
remove(final.db)
gc()

i <- i + 1
saveRDS(i, file = "i.rds")

pid <- Sys.getpid()
saveRDS(pid, file = "pid.rds")

if (i <= (length(file_list))) {
  # if(i <= (length(1:200) ) ) {
  print(paste("Processing of", file_list[i - 1], "ended, restarting"))
  assign(".Last", function() {
    system("Rscript Script200_common.R")
  })
  q(save = "no")
} else {
  print(paste("Processing of", file_list[i - 1], ", so, all ended."))
  q(save = "no")
}
